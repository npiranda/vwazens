extends Node

var game_scene = "res://Scenes/Game.tscn"
var menu_scene = "res://Scenes/Menu.tscn"

onready var menu = get_tree().get_root().get_child(get_tree().get_root().get_child_count()-1)
var game = null

func start_game():
	menu.queue_free()
	var s = ResourceLoader.load(game_scene)
	get_tree().get_root().add_child(s.instance())
	GameManager.startGame()

func game_ended(winner):
	if game == null:
		game = get_tree().get_root().get_child(get_tree().get_root().get_child_count()-1)

	game.queue_free()
	var s = ResourceLoader.load(menu_scene)
	var menu = s.instance()
	get_tree().get_root().add_child(menu)
	menu.displayWinningScene(winner)

