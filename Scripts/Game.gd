extends Node

func _ready():
	var player1 = GameManager.get_first_player_class()
	var player2 = GameManager.get_second_player_class()

	var file = File.new();
	file.open("res://assets/config/playerclass/playerclass.json", file.READ);

	var data = JSON.parse(file.get_as_text());
	
	if data.error == OK:
		get_node("Map/Player1House/Sprite").set_texture(load(data.result[player1].house))
		get_node("Map/Player2House/Sprite").set_texture(load(data.result[player2].house))
	else:
		print("Error when reading the conf... " + data.error_string)
	GameManager.instanciateStatusBars()
