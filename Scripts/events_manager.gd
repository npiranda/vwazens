extends Node

var pool_event;

func init():
	var file = File.new();
	file.open("res://assets/config/events.json", file.READ);
	var text = file.get_as_text();
	pool_event = JSON.parse(text);
	file.close();
	pool_event = pool_event.result;
	
func pullEvent(array):
	var picked = randi() % array.size();
	return array[picked];
	
func pullFromTag(tagName):
	var events_pulled =  [];
	
	for i in pool_event:
		for j in tagName:
			print(j)
			if i.tags.has(j):
				events_pulled.push_back(i);
	if events_pulled.size() > 0:
		return pullEvent(events_pulled);
	return null;
