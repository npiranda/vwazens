extends CanvasLayer

var avatars = {
	"Anatole": preload("res://assets/img/characters/anatole_color.png"),
	"Didier": preload("res://assets/img/characters/didier_color.png"),
	"Sven": preload("res://assets/img/characters/sven_color.png")
}

func _ready():
	var player1 = GameManager.get_first_player_class()
	var player2 = GameManager.get_second_player_class()

	get_node("Container/Player1").set_texture(avatars[player1])
	get_node("Container/Player2").set_texture(avatars[player2])

	var timer = Timer.new()
	timer.connect("timeout", self, "_timeout")
	timer.set_wait_time(3)
	timer.set_one_shot(true)
	add_child(timer)
	timer.start()

func _timeout():
	self.queue_free()