extends Node2D

enum Players {
	Anatole,
	Didier,
	Sven
}

var player1 = null
var player2 = null
var first_player_turn = true

var svenPicked = false
var didierPicked = false
var anatolePicked = false

var avatars = {
	"Anatole": preload("res://assets/img/characters/anatole_color.png"),
	"Didier": preload("res://assets/img/characters/didier_color.png"),
	"Sven": preload("res://assets/img/characters/sven_color.png")
}

onready var animation = get_node("Animation")

func _ready():
	var file = File.new();
	file.open("res://assets/config/playerclass/playerclass.json", file.READ);
	var text = file.get_as_text();
	var data = JSON.parse(text);
	
	if data.error == OK:
		get_node("CanvasLayer/Players/PlayerAnatole/Description").text = data.result["Anatole"].description
		get_node("CanvasLayer/Players/PlayerDidier/Description").text = data.result["Didier"].description
		get_node("CanvasLayer/Players/PlayerSven/Description").text = data.result["Sven"].description

		get_node("CanvasLayer/Players/PlayerAnatole/Stats/Frustration").text = "Frustration : " + String(data.result["Anatole"].initial_stats.frustration)
		get_node("CanvasLayer/Players/PlayerDidier/Stats/Frustration").text = "Frustration : " + String(data.result["Didier"].initial_stats.frustration)
		get_node("CanvasLayer/Players/PlayerSven/Stats/Frustration").text = "Frustration : " + String(data.result["Sven"].initial_stats.frustration)
		get_node("CanvasLayer/Players/PlayerAnatole/Stats/Money").text = "Bifftons : " + String(data.result["Anatole"].initial_stats.money)
		get_node("CanvasLayer/Players/PlayerDidier/Stats/Money").text = "Bifftons : " + String(data.result["Didier"].initial_stats.money)
		get_node("CanvasLayer/Players/PlayerSven/Stats/Money").text = "Bifftons : " + String(data.result["Sven"].initial_stats.money)
	else:
		print("Error when reading the conf... " + data.error_string)

func change_text(text):
	var node = get_node("CanvasLayer/Hint")
	node.text = text
	get_node("HintAnimation").play("HintAnimation")

func _on_Play_pressed():
	get_node("CanvasLayer/Players").show()
	get_node("CanvasLayer/Buttons").hide()
	get_node("CanvasLayer/Title").hide()
	change_text("Player I, choose your character !")

func _on_Quit_pressed():
	get_tree().quit()

func _on_anatole_pressed():
	if anatolePicked: return

	GameManager.registerPlayer("Anatole")
	animation.play("AnatolePressed")
	if first_player_turn:
		anatolePicked = true
		first_player_turn = false
		player1 = Anatole
		change_text("Your turn Player II, choose your character !")
	else:
		player2 = Anatole
		start_game()

func _on_didier_pressed():
	if didierPicked: return

	GameManager.registerPlayer("Didier");
	animation.play("DidierPressed")
	if first_player_turn:
		didierPicked = true
		first_player_turn = false
		player1 = Didier
		change_text("Your turn Player II, choose your character !")
	else:
		player2 = Didier
		start_game()

func _on_sven_pressed():
	if svenPicked: return

	GameManager.registerPlayer("Sven");
	animation.play("SvenPressed")
	if first_player_turn:
		svenPicked = true
		first_player_turn = false
		player1 = Sven
		change_text("Your turn Player II, choose your character !")
	else:
		player2 = Sven
		start_game()

func start_game():
	var timer = Timer.new();
	timer.connect("timeout", self, "_timeout")
	timer.set_wait_time(1)
	timer.set_one_shot(true)
	add_child(timer)
	timer.start()

func _timeout():
	SceneManager.start_game()

func displayWinningScene(winner):
	get_node("CanvasLayer/Players").hide()
	get_node("CanvasLayer/Buttons").hide()
	get_node("CanvasLayer/Title").hide()

	get_node("Winning/Container").show()
	get_node("Winning/Container/Winner").set_texture(avatars[winner])

	var timer = Timer.new();
	timer.connect("timeout", self, "_display_menu")
	timer.set_wait_time(3)
	timer.set_one_shot(true)
	add_child(timer)
	timer.start()

func _display_menu():
	get_tree().quit()

	get_node("CanvasLayer/Buttons").show()
	get_node("CanvasLayer/Title").show()
	get_node("Winning").queue_free()