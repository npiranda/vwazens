extends Node2D

var frustration;
var rage;
var money;
var houseHp;
var profile_picture;
var canPlay = true;
var className;

signal frustration_received(player, dmg)
signal rage_received(player, dmg)

func _ready():
	#init("placeholder"); #used to test, to remove
	pass

func init(className):
	var file = File.new();
	file.open("res://assets/config/playerclass/playerclass.json", file.READ);
	var text = file.get_as_text();
	var data = JSON.parse(text);
	
	self.className = className;
	
	if data.error != OK:
		print("Error while reading file : code ", data.error);
		return;
	if data.result.has(className):
		frustration = data.result[className].initial_stats.frustration;
		rage = data.result[className].initial_stats.rage;
		money = data.result[className].initial_stats.money;
		profile_picture = data.result[className].profile_picture;
		houseHp = 100;
	else:
		print("Player initiated with non existant className : " + className);
	file.close();

func checkDed():
	if money <= 0:
		return 1;
	if frustration >= 100:
		return 2;
	if houseHp <= 0:
		return 3;
		
	return 0;
	