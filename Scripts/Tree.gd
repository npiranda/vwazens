extends Node2D

var timer = null

onready var animation = get_node("Animation")

func _ready():
	timer = Timer.new();
	timer.connect("timeout", self, "_timeout")
	timer.set_wait_time(rand_range(10, 25))
	timer.set_one_shot(false)
	add_child(timer)
	timer.start()

func _timeout():
	animation.play("Move")