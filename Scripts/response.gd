extends TextureButton

var data;
var path = "res://assets/img/cards/"
func init(d):
	data = d;
	var img = Image.new()
	print(str(path, d.asset))
	img.load(str(path, d.asset))
	var itex = ImageTexture.new()
	itex.create_from_image(img)
	itex.set_size_override(Vector2(100, 130));
	texture_normal = itex
	hint_tooltip = str(d.name, " : ", d.description);
func _on_resp_pressed():
	if data.has("effects") :
		var eventEffects = data.effects;
		for i in eventEffects:
			GameManager.applyEffect(i);
	GameManager.hideEvent();
	get_parent().queue_free();
