extends CanvasLayer

signal frustration_received(player, dmg)
signal rage_received(player, dmg)

var avatars = {
	"Anatole": preload("res://assets/img/characters/anatole_color_tiny.png"),
	"Didier": preload("res://assets/img/characters/didier_color_tiny.png"),
	"Sven": preload("res://assets/img/characters/sven_color_tiny.png")
}

var player1 = null
var player2 = null

func _ready():
	player1 = GameManager.get_first_player_class()
	player2 = GameManager.get_second_player_class()

	if player1 == "placeholder":
		player1 = "Sven"
	if player2 == "placeholder":
		player2 = "Didier"

	get_node("Player1/Container/Avatar").set_texture(avatars[player1])
	get_node("Player2/Container/Avatar").set_texture(avatars[player2])

func getEvent(tag):
	var event = EventsManager.pullFromTag(tag);
	
	var assetRoot = "res://assets/img/cards/"
	var parent = get_tree().get_root().find_node("HUD", true, false);
	var hud = parent.get_node("rootResponses");
	var Desc = parent.get_node("rootDescription");
	var sprite = parent.get_node("TextureRect");
	var animations = get_tree().get_root().find_node("Animations", true, false);
	sprite.show();
	hud.show();
	Desc.show();
	Desc.bbcode_text = str("[center]", event.description, "[/center]");
	sprite.set_texture(load(str(assetRoot, event.asset)));

	if event.has("effects") :
		var eventEffects = event.effects;
		for i in eventEffects:
			GameManager.applyEffect(i);
	
	animations.play("EventFadeInOut");
	
	GameManager.nextPlayer();

func _on_KCDQ_pressed():
	getEvent(["atk"]);

func _on_MoneyRegen_pressed():
	getEvent(["moneyRegen"]);

func _on_FrustrationRegen_pressed():
	getEvent(["frustrationRegen"]);

func _on_RageRegen_pressed():
	getEvent(["rageRegen"]);

func initBars(player, frustration, money):
	if player == 1:
		get_node("Player1").get_node("Container").get_node("Bars").get_node("Frustration").set_value(frustration)
		get_node("Player1").get_node("Container").get_node("Bars").get_node("Rage").set_value(0)
		get_node("Player1").get_node("Container").get_node("Bars").get_node("Money").set_text("Bifftons: " + str(money))
	elif player == 2:
		get_node("Player2").get_node("Container").get_node("Bars").get_node("Frustration").set_value(frustration)
		get_node("Player2").get_node("Container").get_node("Bars").get_node("Rage").set_value(0)
		get_node("Player2").get_node("Container").get_node("Bars").get_node("Money").set_text("Bifftons: " + str(money))
	else:
		print("Frustration_received signals references non existing player " + player)

func frustrationReceive(player, dmg):
	var nodeBarre
	print("received");
	if player == 1:
		nodeBarre = get_node("Player1").get_node("Container").get_node("Bars").get_node("Frustration")
	elif player == 2:
		nodeBarre = get_node("Player2").get_node("Container").get_node("Bars").get_node("Frustration")
	nodeBarre.set_value(nodeBarre.get_value() + dmg)

func rageReceive(player, dmg):
	var nodeBarre
	
	if player == 1:
		nodeBarre = get_node("Player1").get_node("Container").get_node("Bars").get_node("Rage")
	elif player == 2:
		nodeBarre = get_node("Player2").get_node("Container").get_node("Bars").get_node("Rage")
	nodeBarre.set_value(nodeBarre.get_value() + dmg)

func moneyReceive(player, newAmount):
	var nodeBarre
	
	if player == 1:
		nodeBarre = get_node("Player1").get_node("Container").get_node("Bars").get_node("Money")
	elif player == 2:
		nodeBarre = get_node("Player2").get_node("Container").get_node("Bars").get_node("Money")
	nodeBarre.set_text("Bifftons: " + String(newAmount))

func houseHPReceive(player, dmg):
	var nodeBarre
	
	if player == 1:
		nodeBarre = get_node("HouseBars/Player1")
	elif player == 2:
		nodeBarre = get_node("HouseBars/Player2")
	else:
		print("Frustration_received signals references non existing player " + player)
	nodeBarre.set_value(nodeBarre.get_value() + dmg)

func greyPlayer(player, fromEnd):
	# fromEnd to true plays the animation backward
	# player to true = player2, false = player1
	print("[Call to greyPlayer("+ String(player) +", "+ String(fromEnd) +")")
	var nodeAnimationPlayer = get_node("GreyPlayer1")
	var nodeAnimationPlayer2 = get_node("GreyPlayer2")
	if player:
		if fromEnd:
			nodeAnimationPlayer2.play_backwards("Second_Player_Greyed")
		else:
			nodeAnimationPlayer2.play("Second_Player_Greyed")
	else:
		if fromEnd:
			nodeAnimationPlayer.play_backwards("First_Player_Greyed")
		else:
			nodeAnimationPlayer.play("First_Player_Greyed")
